# Content

This directory contains Cram based tests.

## generic

Contains tests which could be used on all devices.

## turris-omnia

Contains tests which could be used against `Turris Omnia` device.

## glinet-b1300

Contains tests which could be used against `Gl.iNet B1300` device.

## nec-wx3000hp

Contains tests which could be used against `NEC WX3000HP` device.
